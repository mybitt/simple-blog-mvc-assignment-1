// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var storeSchema = new Schema({
    user: String,
    title: String,
    content: String
});
// the schema is useless so far
// we need to create a model using it
var blog = mongoose.model('blog', storeSchema);
// make this available to our users in our Node applications
module.exports = blog;